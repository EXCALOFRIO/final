/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dominio;
/**
 * Clase Zapatilla del programa con getters, setters, constructor y metodo toString
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */
public class Zapatilla {
	private String modelo;
	private String marca;
	private int talla =0;
	private int id = 0;
	
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return modelo  El modelo de la zapatilla
     */
	public String getModelo() {
		return modelo;
	}
	
	/**
     * Metodo setter que sirve para asignar un valor a una variable modelo
     * @param modelo  El modelo de la zapatilla
     */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return marca  La marca de la zapatilla
     */
	public String getMarca() {
		return marca;
	}
	
	/**
     * Metodo setter que sirve para asignar un valor a una variable marca
     * @param marca  La marca de la zapatilla
     */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return talla  La talla de la zapatilla
     */
	public int getTalla() {
		return talla;
	}
	/**
     * Metodo setter que sirve para asignar un valor a una variable talla
     * @param talla  La talla de la zapatilla
     */
	public void setTalla(int talla) {
		this.talla = talla;
	}
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return id  El id de la zapatilla
     */
	public int getId() {
		return id ;
	}
	/**
     * Metodo setter que sirve para asignar un valor a una variable talla
     * @param id  El id de la zapatilla
     */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
     * Constructor Es el construnctor de zapatilla con los atributos id, marca, modelo y talla
     * @param id  Es el id de la zapatilla
     * @param modelo  Es el modelo de la zapatilla
     * @param marca  Es la marca de la zapatilla
     * @param talla  Es la talla de la zapatilla
     */	
	public Zapatilla(int id, String modelo, String marca , int talla){
		this.id = id;
		this.marca = marca;
		this.modelo = modelo;
		this.talla = talla;
	}
	
	/**
     * Constructor Es el construnctor de zapatilla con el atributo id, solo uno para poder borrar una zapatilla solo con el id.
     * @param id Es el id de la zapatilla
     */	
	public Zapatilla(int id){
		this.id = id;
	}
	/**
	 * Metodo toString que devuelve los atributos de zapatilla
	 * @return id  Es el id de la zapatilla
	 * @return modelo  Es el modelo de la zapatilla
     * @return marca  Es la marca de la zapatilla
     * @return talla  Es la talla de la zapatilla
	 */	
	public String toString() {
		return "ID:" + " " + id + " " + "Tipo:" + " " + "NORMAL" + " " + "Modelo:" + " " + modelo + " " + "Marca:" + " "+ marca + " " + "Talla:" + " " + talla + "\n";
	}
	
	/**
	 * Metodo toCSV que crea las zpatillas entre comillas para el csv
	 * @return id  Es el id de la zapatilla
	 * @return modelo  Es el modelo de la zapatilla
     * @return marca  Es la marca de la zapatilla
     * @return talla  Es la talla de la zapatilla
	 */	
	public String toCSV() {
		return "ID:" + "," + id + "," + "Tipo:" + "," + "NORMAL" + "," + "Modelo:" + "," + modelo + "," + "Marca:" + "," + marca + "," + "Talla:" + "," + talla + "\n";
	}
	
	/**
	 * Metodo equals que compara los id para ver si son iguales o no
	 * @param object
	 * @return false  si el id es distinto
	 * @return true  si el id es igual
	 */	
	public boolean equals(Object object){
		Zapatilla zapatilla = (Zapatilla) object;
		if(id == zapatilla.getId()){
			return true;
		} else {
			return false;
		}
	}
}
