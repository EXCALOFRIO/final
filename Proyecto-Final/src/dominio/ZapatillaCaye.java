/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dominio;
/**
 * Clase ZapatillaCaye que contiene los getters setters csv y el toString de la misma
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */
public class ZapatillaCaye extends Zapatilla{
	private String cordones;
	
	/**
     * Constructor Es el construnctor de zapatillaCaye con los atributos id, cordones, modelo y talla
     * @param id  Es el id de la zapatillaCaye
     * @param modelo  Es el modelo de la zapatillaCaye
     * @param cordones  Son los cordones de la zapatillaCaye
     * @param talla Es  la talla de la zapatillaCaye
     */			
	public ZapatillaCaye(int id,String cordones ,String modelo, int talla){
		super(id, modelo, " " , talla);
		this.cordones = cordones;	
	}
	/**
	* Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
	* @return cordones  Los cordones de la zapatilla
	*/
	public String getCordones() {
		return cordones;
	}
	/**
    * Metodo setter que sirve para asignar un valor a una variable cordones
    * @param cordones  Los cordones de la zapatilla
    */
	public void setCordones(String cordones) {
		this.cordones = cordones;
	}
	//Anula el metodo principal toString de Zapatilla para asi poder usar uno especifico
	@Override
	/**
	 * Metodo toString que devuelve los atributos de zapatillaCaye
	 * @return id  Es el id de la zapatillaCaye
	 * @return modelo  Es el modelo de la zapatillaCaye
     * @return cordones  Es la cordones de la zapatillaCaye
     * @return talla  Es la talla de la zapatillaCaye
	 */	
	public String toString() {
		return "ID:" + " " + getId() + " " + "Tipo:" + " " + "CAYE" + " " + "Modelo:" + " " + getModelo() + " " + "Cordones:" + " " + cordones + " " + "Talla:" + " " + getTalla() + "\n";
	}
	
	@Override
	/**
	 * Metodo toCSV que crea las zpatillasCaye entre comillas para el csv
	 * @return id Es el id de la zapatillaCaye
	 * @return modelo Es el modelo de la zapatillaCaye
     * @return cordones Es la cordones de la zapatillaCaye
     * @return talla Es la talla de la zapatillaCaye
	 */	
	public String toCSV() {
		return "ID:" + "," + getId() + "," + "Tipo:" + "," + "CAYE" + "," + "Modelo:" + "," + getModelo() + "," + "Cordones:" + "," + cordones + "," + "Talla:" + "," + getTalla() + "\n";
	}
}
