/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dominio;
/**
 * Clase Catalogo que contiene el ArrayList de zapatillas y los metodos para annadir eliminar o editar 
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */
import java.util.ArrayList;

public class Catalogo {
	private ArrayList<Zapatilla> coleccionZapatillas = new ArrayList<>();
	
	/**
	 * Metodo annadirZapatilla metodo void que annade zapatillas
	 * @param zapatilla  Zapatilla que annade al catalogo
	 */
	public void annadirZapatilla(Zapatilla zapatilla){
		coleccionZapatillas.add(zapatilla);
	}
	
	/**
	 * Metodo eliminarZapatilla metodo void que elimina zapatillas
	 * @param zapatillaEliminar  Zapatilla que annade al catalogo
	 */
	public void eliminarZapatilla(Zapatilla zapatillaEliminar){
		coleccionZapatillas.remove(zapatillaEliminar);
	}
	
	/**
	 * Metodo editarZapatillaAnnadir metodo void que annade zapatillas
	 * @param zapatillaEditar  Zapatilla que annade al catalogo
	 */
	public void editarZapatillaAnnadir(Zapatilla zapatillaEditar){
		coleccionZapatillas.add(zapatillaEditar);
	}
	
	/**
	 * Metodo editarZapatillaEliminar metodo void que elimina zapatillas
	 * @param zapatillaEditarEliminada  Zapatilla que annade al catalogo
	 */
	public void editarZapatillaEliminar(Zapatilla zapatillaEditarEliminada){
		coleccionZapatillas.remove(zapatillaEditarEliminada);
	}
	
	/**
	 * Metodo toString que devuelve los datos de la zapatilla, se annaden al arraylist
	 * @return datos  Datos de la zapatilla
	 */
	public String toString(){
		StringBuilder datos = new StringBuilder();
		for (Zapatilla zapatilla : coleccionZapatillas){
			datos.append(zapatilla);
		}
		return datos.toString();
	}
	
	/**
	 * Metodo toCSV que devuelve los datos de la zapatilla, se annaden al arraylist de la forma csv
	 * @return datos  Datos de la zapatilla
	 */
	public String toCSV(){
		StringBuilder datos = new StringBuilder();
		for (Zapatilla zapatilla : coleccionZapatillas){
			datos.append(zapatilla.toCSV());
		}
		return datos.toString();
	}
	
	/**
	 * Metodo getContador encargado de sacar el tamanno del ArrayList para luego usarlo en los id y que no se repitan.
	 * @return getContador Tamanno del ArrayList para el id.
	 */
	public int getContador() {
		if(coleccionZapatillas.size() == 0) {
			return 1;
		}else {
			return coleccionZapatillas.get(coleccionZapatillas.size()-1).getId()+1;
		}
	}
}
