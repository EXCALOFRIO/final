/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package dominio;
/**
 * Clase ZapatillaDeportiva que contiene los getters setters csv y el toString de la misma
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */

//extends nos permite añadir la subclase ZapatillaDeportiva a la superclase Zapatilla
public class ZapatillaDeportiva extends Zapatilla{
	private String amortiguador;
	
	/**
     * Constructor  Es el construnctor de zapatillaDeportiva con los atributos id, amortiguador, modelo y talla
     * @param id  Es el id de la zapatillaDeportiva
     * @param modelo  Es el modelo de la zapatillaDeportiva
     * @param amortiguador  Son los amortiguadores de la zapatillaDeportiva
     * @param talla  Es la talla de la zapatillaDeportiva
     */	
	public ZapatillaDeportiva(int id, String modelo,String amortiguador, int talla){
		super(id, modelo, " " , talla);
		this.amortiguador = amortiguador;
	}
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return amortiguador  El amortiguador de la zapatilla
     */	
	public String getAmortiguador() {
		return amortiguador;
	}
	/**
     * Metodo setter que sirve para asignar un valor a una variable amortiguador
     * @param amortiguador  El amortiguador de la zapatilla
     */
	public void setAmortiguador(String amortiguador) {
		this.amortiguador = amortiguador;
	}
	
	//Anula el metodo principal toString de Zapatilla para asi poder usar uno especifico.
	@Override
	/**
	 * Metodo toString que devuelve los atributos de zapatillaDeportiva
	 * @return id  Es el id de la zapatillaDeportiva
	 * @return modelo  Es el modelo de la zapatillaDeportiva
     * @return amortiguacion  Es la amortiguacion de la zapatillaDeportiva
     * @return talla  Es la talla de la zapatillaDeportiva
	 */	
	public String toString() {
		return "ID:" + " " + getId() + " " + "Tipo:" + " " + "DEPORTIVA" + " " + "Modelo:" + " " + getModelo() + " " + "Amortiguacion:" + " "+ amortiguador + " " + "Talla:" + " " + getTalla() + "\n";
	}
	
	@Override
	/**
	 * Metodo toCSV que crea las zpatillasDeportivas entre comillas para el csv
	 * @return id  Es el id de la zapatillaDeportiva
	 * @return modelo  Es el modelo de la zapatillaDeportiva
     * @return amortiguacion  Es la amortiguacion de la zapatillaDeportiva
     * @return talla  Es la talla de la zapatillaDeportiva
	 */
	public String toCSV() {
		return "ID:" + "," + getId() + "," + "Tipo:" + "," + "DEPORTIVA" + "," + "Modelo:" + "," + getModelo() + "," + "Amortiguacion:" + ","+ amortiguador + "," + "Talla:" + "," + getTalla() + "\n";
	}
}
