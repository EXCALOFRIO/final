/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

package aplicacion;
import interfaz.Interfaz;
/**
 * Clase Principal del programa que procesa la peticion
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */
public class Principal{
	/**
	 * Metodo que procesa la peticion de la sentencia
	 *@param  args Entrada del primer comando que ejecutar como por ejemplo add, calatalogo o help
	 */
	public static void main(String[] args){
		String sentencia = "";
		for(int i = 0; i< args.length; i++){
			sentencia += args[i] + " ";
		}
		Interfaz.procesarPeticion(sentencia);
	}
}

