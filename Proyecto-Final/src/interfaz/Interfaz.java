/*
Copyright [2020] [Alejandro Ramirez Larena, Javier Martinez Cristobal, Pablo Rivero, Joaquin Moreno Guzman copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package interfaz;
/**
 * Clase Interfaz contiene el metodo que inicializa el archivo txt y el catalogo, metodo que procesa la peticion con comandos de ayuda.
 * @author Javier Martinez
 * @author Alejandro Ramirez
 * @author Pablo Rivero
 * @author Joaquin Moreno
 * @version 21.2, 16/12/2020
 */
import dominio.*;

import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;


public class Interfaz {
	
	private static int contadorId = 0;
	/**
     * Metodo getter que sirve para devolver el valor del atributo a quien lo solicite
     * @return contadorId El id de la zapatilla
     */
	public static int getContadorId() {
		return contadorId;
	}
	/**
     * Metodo setter que sirve para asignar un valor a una variable talla
     * @param contadorId  El id de la zapatilla
     */
	public static void setContadorId(int contadorId) {
		Interfaz.contadorId = contadorId;
	}
	
	private static String HELP_TEXT ="-De esta forma se annaden unas zapatillas java -jar catalogo.jar add Jordan Nike 42\n"+
			"-De esta forma se annaden unas zapatillasDeportivas java -jar catalogo.jar addDeportivas Jordan Nike 42\n"+
			"-De esta forma se annaden unas zapatillasCaye java -jar catalogo.jar addCaye Jordan Nike 42\n"+
			"-De esta forma se eliminan zapatillas java -jar catalogo.jar delete 1\n"+ 
			"-De esta forma se editan zapatillas java -jar catalogo.jar editNormal 1 Jordan Nike 43\n"+ 
			"-De esta forma se editan zapatillas java -jar catalogo.jar editDeportiva 1 Adidas Superstar 43\n"+ 
			"-De esta forma se editan zapatillas java -jar catalogo.jar editCaye 1 Nautico SinCordones 43\n"+ 
			"-De esta forma se ve el catalogo java -jar catalogo.jar catalogo\n"+ 
			"-De esta forma se muestra la ayuda java -jar catalogo.jar help java -jar catalogo.jar help\n"+ 
			"-De esta forma se genera el CSV java -jar catalogo.jar csv";
	
	private static String NOMBRE_FICHERO = "catalogo.txt";
	private static String NOMBRE_CSV = "catalogoCSV.csv";
	
	/**
	 * Metodo inicializarFichero  metodo estatico que inicializa el archivo txt
	 * @param catalogo  Importa el catalogo para annadirlo luego en el archivo txt
	 */
	private static void inicializarFichero(Catalogo catalogo){
		try{
			FileWriter fw = new FileWriter(NOMBRE_FICHERO);
			
			fw.write(catalogo.toString());
			fw.close();
		} catch (Exception e){
			System.out.println(e);
			System.out.println("No se ha podido guardar el archivo");
		}
	}
	
	/**
	 * Metodo generarCSV  metodo estatico que inicializa el archivo csv
	 * @param catalogo  Importa el catalogo para annadirlo luego en el archivo csv
	 */
	private static void generarCSV(Catalogo catalogo){
		try{
			FileWriter fw = new FileWriter(NOMBRE_CSV);
			fw.write(catalogo.toCSV());
			fw.close();
		} catch (Exception e){
			System.out.println(e);
			System.out.println("No se ha podido guardar el archivo");
		}
	}
	
	/**
	 * Metodo estatico que inicializa el  catalogo
	 * @param NOMBRE_FICHERO  Es el nombre que tiene el fichero donde se almacenan los datos
	 * @return catalogo  Catalogo de zapatillas con todos los modelos distintos
	 */
	private static Catalogo inicializarCatalogo(String NOMBRE_FICHERO){
		Catalogo catalogo = new Catalogo();
		try{
			File file = new File(NOMBRE_FICHERO);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
				String bonito1 =sc.next();
				int id = sc.nextInt();
				String bonito2 = sc.next();
				String tipo = sc.next();
				String bonito3 =sc.next();
				String modelo = sc.next();
				
				if (tipo.equals("NORMAL")){
					String bonito4 =sc.next();
					String marca = sc.next();
					String bonito5 =sc.next();
					int talla = sc.nextInt();
					Zapatilla zapatilla = new Zapatilla(id , modelo , marca , talla);
					catalogo.annadirZapatilla(zapatilla);
				}if(tipo.equals("DEPORTIVA")){
					String bonito4 =sc.next();
					String amortiguador = sc.next();
					String bonito5 =sc.next();
					int talla = sc.nextInt();
					Zapatilla zapatilla = new ZapatillaDeportiva(id  , modelo , amortiguador , talla);
					catalogo.annadirZapatilla(zapatilla);
				}if(tipo.equals("CAYE")){
					String bonito4 =sc.next();
					String cordones = sc.next();
					String bonito5 =sc.next();
					int talla = sc.nextInt();
					Zapatilla zapatilla = new ZapatillaCaye(id , modelo , cordones ,talla);
					catalogo.annadirZapatilla(zapatilla);
				}
			}
			sc.close();
		} catch (FileNotFoundException e){
			inicializarFichero(catalogo);
			} catch (Exception e){
			System.out.println(e);
			System.out.println("No se ha podido inicializar el fichero");
			}
		return catalogo;
	}
	
	/**
	 * Metodo procesarPeticion  metodo estatico que procesa el argumento introducido y establece una serie de comandos como help, catalogo o add
	 *@param sentencia  Se encuentra en principal
	 */
	public static void procesarPeticion(String sentencia){
		String[] args = sentencia.split(" ");
		Catalogo catalogo = inicializarCatalogo(NOMBRE_FICHERO);
		switch (args[0]) {
		case "catalogo":
			if(catalogo.toString().equals("")){
				System.out.println("No hay ningun producto en el catalogo.");
				System.out.println(catalogo);
			} else {
				System.out.println(catalogo);
			}
			break;
			
		case "add":
			try {
				try {
					Integer.parseInt(args[1]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[2]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
					}catch(Exception e1){
						contadorId = catalogo.getContador();
						Zapatilla zapatilla = new Zapatilla(contadorId, args[1], args[2] , Integer.parseInt(args[3]));
						catalogo.annadirZapatilla(zapatilla);
						inicializarFichero(catalogo);
					}
				}
			}catch(Exception e2){
				System.out.println(e2);
				System.out.println("La forma en la que has intentado annadir una zapatilla es incorrecta, preube el comando help para mas info");
			}
			break;
			
		case "addDeportiva":
			try {
				try {
					Integer.parseInt(args[1]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[2]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
					}catch(Exception e1){
						contadorId = catalogo.getContador();
						catalogo.annadirZapatilla(new ZapatillaDeportiva(contadorId, args[1] , args[2],  Integer.parseInt(args[3])));
						inicializarFichero(catalogo);	
					}
				}
			}catch(Exception e2){
				System.out.println(e2);
				System.out.println("La forma en la que has intentado annadir una zapatilla es incorrecta, preube el comando help para mas info");
			}
			break;
			
		case "addCaye":
			try {
				try {
					Integer.parseInt(args[1]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[2]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
					}catch(Exception e1){
						contadorId = catalogo.getContador();
						catalogo.annadirZapatilla(new ZapatillaCaye(contadorId ,args[1] , args[2], Integer.parseInt(args[3])));
						inicializarFichero(catalogo);
					}
				}
			}catch(Exception e2){
				System.out.println(e2);
				System.out.println("La forma en la que has intentado annadir una zapatilla es incorrecta, preube el comando help para mas info");
			}	
			break;
			
		case "help":
			System.out.println(HELP_TEXT);
			break;
			
		case "csv":
			generarCSV(catalogo);
			break;
			
		case "delete":
			try {
				catalogo.eliminarZapatilla(new Zapatilla(Integer.parseInt(args[1])));
				inicializarFichero(catalogo);
			}catch(Exception e){
				System.out.println(e);
				System.out.println("La forma en la que has intentado annadir una zapatilla es incorrecta, preube el comando help para mas info");
			}
			break;
			
		case "editNormal":
			try {
				try {
					Integer.parseInt(args[2]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[3]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
							}catch(Exception e3){
								catalogo.eliminarZapatilla(new Zapatilla(Integer.parseInt(args[1])));
								inicializarFichero(catalogo);
								contadorId = catalogo.getContador();
								catalogo.annadirZapatilla(new Zapatilla(contadorId , args[2], args[3] , Integer.parseInt(args[4])));
								inicializarFichero(catalogo);
							}
				}
			}catch(Exception e){
				System.out.println(e);
				System.out.println("La forma en la que has intentado annadir una zpatilla es incorrecta, preube el comando help para mas info");
			}
			break;
			
		case "editDeportiva":
			try {
				try {
					Integer.parseInt(args[2]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[3]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
							}catch(Exception e3){
								catalogo.eliminarZapatilla(new Zapatilla(Integer.parseInt(args[1])));
								inicializarFichero(catalogo);
								contadorId = catalogo.getContador();
								catalogo.annadirZapatilla(new ZapatillaDeportiva(contadorId , args[2], args[3] , Integer.parseInt(args[4])));
								inicializarFichero(catalogo);
							}
				}
			}catch(Exception e){
				System.out.println(e);
				System.out.println("La forma en la que has intentado annadir una zpatilla es incorrecta, preube el comando help para mas info");
			}
			break;
			
		case "editCaye":
			try {
				try {
					Integer.parseInt(args[2]);
					System.out.println("La forma en la que has intentado annadir el segundo dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
				}catch(Exception e){
					try {
						Integer.parseInt(args[3]);
						System.out.println("La forma en la que has intentado annadir el tercer dato, te recuerdo que tiene que ser de tipo texto no numero, preube el comando help para mas info");
							}catch(Exception e3){
								catalogo.eliminarZapatilla(new Zapatilla(Integer.parseInt(args[1])));
								inicializarFichero(catalogo);
								contadorId = catalogo.getContador();
								catalogo.annadirZapatilla(new ZapatillaCaye(contadorId , args[2], args[3] , Integer.parseInt(args[4])));
								inicializarFichero(catalogo);
							}
				}
			}catch(Exception e){
				System.out.println(e);
				System.out.println("La forma en la que has intentado annadir una zpatilla es incorrecta, preube el comando help para mas info");
			}
			break;
		}
	}
}
